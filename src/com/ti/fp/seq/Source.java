package com.ti.fp.seq;

import java.util.ArrayList;
import java.util.Iterator;

import static com.ti.fp.seq.Seq.cons;
import static com.ti.fp.seq.Seq.empty;

final public class Source {
    public static <T> Seq<T> fromArray(T[] arr, int pos) {
        return arr.length == pos ? empty() : cons(arr[pos], () -> fromArray(arr, pos + 1));
    }

    public static <T> Seq<T> fromArray(T[] arr) {
        return fromArray(arr, 0);
    }

    public static <T> Seq<T> fromArrayList(ArrayList<T> arr, int pos) {
        return arr.size() == pos ? empty() : cons(arr.get(pos), () -> fromArrayList(arr, pos + 1));
    }

    public static <T> Seq<T> fromArrayList(ArrayList<T> arr) {
        return fromArrayList(arr, 0);
    }

    public static <T> Seq<T> fromIterable(Iterable<T> iterable) {
        return fromIterator(iterable.iterator());
    }

    private static <T> Seq<T> fromIterator(Iterator<T> iterator) {
        return iterator.hasNext() ? cons(iterator.next(), () -> fromIterator(iterator)) : empty();
    }
}
