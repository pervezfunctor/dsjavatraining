package com.ti.fp.seq;

import java.util.function.Supplier;

import static com.ti.fp.seq.Sink.toList;
import static java.util.stream.Collectors.toList;

final class Cons<T> implements Seq<T> {
    private final T first;
    private final Supplier<Seq<T>> rest;
    private Seq<T> tail_ = null;

    Cons(T head, Supplier<Seq<T>> tail) {
        this.first = head;
        this.rest = tail;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public T head() {
        return first;
    }

    @Override
    public Seq<T> tail() {
        if (tail_ == null)
            tail_ = rest.get();

        return tail_;
    }

    @Override
    public String toString() {
        var elements = toList(this)
                .stream()
                .map(x -> x.toString())
                .collect(toList());

        return "[" + String.join(", ", elements) + "]";
    }
}
