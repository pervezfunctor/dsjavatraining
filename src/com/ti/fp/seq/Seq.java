package com.ti.fp.seq;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public interface Seq<T> {
    static Seq empty() {
        return Empty.empty;
    }

    static <E> Seq<E> cons(E head, Supplier<Seq<E>> tail) {
        return new Cons(head, tail);
    }

    boolean isEmpty();

    T head();

    Seq<T> tail();

    default int length() {
        if (isEmpty())
            return 0;

        return 1 + tail().length();
    }

    default <R> Seq<R> map(Function<T, R> f) {
        if (isEmpty())
            return empty();

        return cons(f.apply(head()), () -> tail().map(f));
    }

    default Seq<T> filter(Predicate<T> predicate) {
        if (isEmpty())
            return empty();

        if (predicate.test(head()))
            return cons(head(), () -> tail().filter(predicate));

        return tail().filter(predicate);
    }
}


