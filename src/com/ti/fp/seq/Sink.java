package com.ti.fp.seq;

import java.util.ArrayList;
import java.util.List;

final public class Sink {
    public static <T> List<T> toList(Seq<T> list) {
        ArrayList<T> arr = new ArrayList<>();

        for (; !list.isEmpty(); list = list.tail())
            arr.add(list.head());

        return arr;
    }
}
