package com.ti.fp.seq;

final class Empty implements Seq {

    public static Empty empty = new Empty();

    private Empty() {

    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public Object head() {
        throw new RuntimeException("head not defined on Empty");
    }

    @Override
    public Seq tail() {
        throw new RuntimeException(("tail not defined on an Empty list"));
    }

    @Override
    public String toString() {
        return "[]";
    }
}
