package com.ti.fp.expr;

import java.math.BigInteger;

public class BigIntegerArithmetic implements Arithmetic<BigInteger> {
    @Override
    public BigInteger add(BigInteger x, BigInteger y) {
        return x.add(y);
    }

    @Override
    public BigInteger subtract(BigInteger x, BigInteger y) {
        return x.subtract(y);
    }

    @Override
    public BigInteger negate(BigInteger x) {
        return x.negate();
    }
}
