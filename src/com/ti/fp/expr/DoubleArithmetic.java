package com.ti.fp.expr;

public class DoubleArithmetic implements Arithmetic<Double> {
    @Override
    public Double add(Double x, Double y) {
        return x + y;
    }

    @Override
    public Double subtract(Double x, Double y) {
        return x - y;
    }

    @Override
    public Double negate(Double x) {
        return -x;
    }
}
