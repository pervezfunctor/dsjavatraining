package com.ti.fp.expr;

public interface Expr<E> {
    E eval();
}
