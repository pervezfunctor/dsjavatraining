package com.ti.fp.expr;

public class Expression<E> {
    private final Arithmetic<E> arithmetic;

    public Expression(Arithmetic<E> arithmetic) {
        this.arithmetic = arithmetic;
    }

    public Expr<E> plus(Expr<E> fst, Expr<E> snd) {
        return () -> arithmetic.add(fst.eval(), snd.eval());
    }

    public Expr<E> minus(Expr<E> fst, Expr<E> snd) {
        return () -> arithmetic.subtract(fst.eval(), snd.eval());
    }

    public Expr<E> negate(Expr<E> expr) {
        return () -> arithmetic.negate(expr.eval());
    }

    public Expr<E> plus(E fst, E snd) {
        return () -> arithmetic.add(fst, snd);
    }

    public Expr<E> minus(E fst, E snd) {
        return () -> arithmetic.subtract(fst, snd);
    }

    public Expr<E> negate(E expr) {
        return () -> arithmetic.negate(expr);
    }

    public Expr<E> op(E value) {
        return () -> value;
    }
}

