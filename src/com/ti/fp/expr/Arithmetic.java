package com.ti.fp.expr;

public interface Arithmetic<E> {
    E add(E x, E y);

    E subtract(E x, E y);

    E negate(E x);
}
