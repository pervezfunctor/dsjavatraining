package com.ti.fp.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Reverse {
    public static <E, Coll extends Collection<E>> Coll reversed(Reversible<E> iterable, Appendable<E, Coll> appendable) {
        var arr = appendable.newCollection();

        for (var iterator = iterable.reverseIterator(); iterator.hasPrevious(); )
            appendable.append(arr, iterator.previous());

        return arr;
    }

    public static <E> ArrayList<E> reversed(Reversible<E> iterable) {
        return reversed(iterable, Appendable.arrayAppender());
    }

    public static <E> ArrayList<E> reversed(List<E> list) {
        return reversed(Reversible.from(list), Appendable.arrayAppender());
    }

    // reversed is impossible to write correctly for an array

    private static <E> void swap(E[] arr, int i, int j) {
        var t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    private static void swap(int[] arr, int i, int j) {
        var t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    private static void swap(double[] arr, int i, int j) {
        var t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    private static void swap(long[] arr, int i, int j) {
        var t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    public static void reverse(int[] list) {
        var i = 0;
        var j = list.length;

        while (i != j && --j != i) {
            swap(list, i, j);
            ++i;
        }
    }

        public static void reverse(double[] list) {
        var i = 0;
        var j = list.length;

        while (i != j && --j != i) {
            swap(list, i, j);
            ++i;
        }
    }

    public static void reverse(long[] list) {
        var i = 0;
        var j = list.length;


        while (i != j && --j != i) {
            swap(list, i, j);
            ++i;
        }
    }

    public static <E> void reverse(E[] list) {
        reverse(new ArrayReversible<>(list));
    }

    public static <E> void reverse(List<E> list) {
        reverse(new ListReversible<>(list));
    }

    public static <E> void reverse(Reversible<E> list) {
        var first = list.iterator();
        var last = list.reverseIterator();

        while (last.previousIndex() != first.nextIndex() &&
                last.previousIndex() + 1 != first.nextIndex()) {
            var n = first.next();
            var p = last.previous();

            first.set(p);
            last.set(n);
        }
    }
}
