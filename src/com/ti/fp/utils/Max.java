package com.ti.fp.utils;

import static com.ti.fp.utils.CompareResult.LESS;

public class Max {
    public static <E extends MyComparable<E>> E max(E x, E y) {
        return x.compareTo(y) == LESS ? y : x;
    }

    public static <E extends MyComparable<E>> E max(E[] arr) {
        return max(new ArrayIterable<>(arr));
    }

    public static int max(int[] arr) {
        var max = arr[0];

        for (var i = 1; i < arr.length; ++i)
            if (max < arr[i])
                max = arr[i];

        return max;
    }

    public static double max(double[] arr) {
        var max = arr[0];

        for (var i = 1; i < arr.length; ++i)
            if (max < arr[i])
                max = arr[i];

        return max;
    }

    public static long max(long[] arr) {
        var max = arr[0];

        for (var i = 1; i < arr.length; ++i)
            if (max < arr[i])
                max = arr[i];

        return max;
    }

    public static <E extends MyComparable<E>> E max(Iterable<E> arr) {
        return max(arr, (x, y) -> x.compareTo(y));
    }

    public static <E> E max(Iterable<E> iterable, MyComparator<E> cmp) {
        var iterator = iterable.iterator();

        if (!iterator.hasNext())
            throw new RuntimeException("max not defined for an empty iterable");

        var max = iterator.next();

        while (iterator.hasNext()) {
            var value = iterator.next();
            if (cmp.compare(max, value) == CompareResult.LESS)
                max = value;
        }

        return max;
    }
}

// Head first Java
// The Java Programming Language - James Gosling
// Head first Design Patterns
// Modern Java in Action
// Effective Java
