package com.ti.fp.utils;

import java.util.Iterator;

public class ArrayIterable<E> implements Iterable<E> {
    private final E[] arr;

    public ArrayIterable(E[] arr) {
        this.arr = arr;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<>() {
            private int pos;

            @Override
            public boolean hasNext() {
                return pos < arr.length - 1;
            }

            @Override
            public E next() {
                return arr[pos++];
            }
        };
    }
}
