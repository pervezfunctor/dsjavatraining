package com.ti.fp.utils;

import java.util.List;
import java.util.ListIterator;

public interface Reversible<E> extends Iterable<E> {
    static <E> Reversible<E> from(List<E> list) {
        return new Reversible<>() {
            @Override
            public ListIterator<E> reverseIterator() {
                return list.listIterator(list.size());
            }

            @Override
            public ListIterator<E> iterator() {
                return list.listIterator();
            }
        };
    }

    ListIterator<E> reverseIterator();
    ListIterator<E> iterator();
}
