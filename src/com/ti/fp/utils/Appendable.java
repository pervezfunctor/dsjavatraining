package com.ti.fp.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public interface Appendable<E, Coll extends Collection<E>> {
    static <E, Coll extends Collection<E>> Appendable<E, Coll> from(Supplier<Coll> newCollection, BiConsumer<Coll, E> append) {
        return new Appendable<>() {
            @Override
            public Coll newCollection() {
                return newCollection.get();
            }

            @Override
            public void append(Coll es, E x) {
                append.accept(es, x);
            }
        };
    }

    static <E> Appendable<E, ArrayList<E>> arrayAppender() {
        return from(
                () -> new ArrayList<>(),
                (coll, e) -> coll.add(e));
    }

    static <E> Appendable<E, LinkedList<E>> linkedAppender() {
        return from(
                () -> new LinkedList<>(),
                (coll, e) -> coll.add(e));
    }

    Coll newCollection();

    void append(Coll coll, E x);
}
