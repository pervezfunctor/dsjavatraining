package com.ti.fp.utils;

import java.util.Comparator;

@FunctionalInterface
public interface MyComparator<E> {
    CompareResult compare(E x, E y);

    static <E> MyComparator<E> from(Comparator<E> cmp) {
        return (x, y) -> CompareResult.from(cmp.compare(x, y));
    }
}
