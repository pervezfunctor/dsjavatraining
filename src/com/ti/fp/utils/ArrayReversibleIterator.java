package com.ti.fp.utils;

import java.util.ListIterator;

enum Called { NONE, NEXT, PREVIOUS}

public class ArrayReversibleIterator<E> implements ListIterator<E> {
    private final E[] arr;
    private int pos;
    private Called called = Called.NONE;

    public ArrayReversibleIterator(E[] arr, int pos) {
        this.arr = arr;
        this.pos = pos;
    }

    public ArrayReversibleIterator(E[] arr) {
        this(arr, 0);
    }

    @Override
    public boolean hasNext() {
        return pos < arr.length;
    }

    @Override
    public E next() {
        called = Called.NEXT;
        return arr[pos++];
    }

    @Override
    public boolean hasPrevious() {
        called = Called.PREVIOUS;
        return pos > 0;
    }

    @Override
    public E previous() {
        return arr[--pos];
    }

    @Override
    public int nextIndex() {
        return pos;
    }

    @Override
    public int previousIndex() {
        return pos - 1;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("array is fixed size, cannot support remove");
    }

    @Override
    public void set(E e) {
        if(called == Called.NEXT)
            arr[pos - 1] = e;
        else if(called == Called.PREVIOUS)
            arr[pos] = e;
        else
            throw new RuntimeException("call next or previous before set");
    }

    @Override
    public void add(E e) {
        throw new UnsupportedOperationException("array is fixed size, cannot support add");
    }
}
