package com.ti.fp.utils;

import java.util.List;
import java.util.ListIterator;

public class ListReversible<E> implements Reversible<E> {
    private final List<E> list;

    public ListReversible(List<E> list) {
        this.list = list;
    }

    @Override
    public ListIterator<E> reverseIterator() {
        return list.listIterator(list.size());
    }

    @Override
    public ListIterator<E> iterator() {
        return list.listIterator(0);
    }
}
