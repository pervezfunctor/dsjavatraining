package com.ti.fp.utils;

@FunctionalInterface
public interface MyComparable<E> {
    CompareResult compareTo(E x);

    static <E> MyComparable<E> from(Comparable<E> x) {
        return y -> CompareResult.from(x.compareTo(y));
    }
}
