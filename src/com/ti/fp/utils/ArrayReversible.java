package com.ti.fp.utils;

import java.util.ListIterator;

public class ArrayReversible<E> implements Reversible<E> {
    private final E[] arr;
    public ArrayReversible(E[] arr) {
        this.arr = arr;
    }

    @Override
    public ListIterator<E> reverseIterator() {
        return new ArrayReversibleIterator<>(arr, arr.length);
    }

    @Override
    public ListIterator<E> iterator() {
        return new ArrayReversibleIterator<>(arr);
    }
}
