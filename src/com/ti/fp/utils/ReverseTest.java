package com.ti.fp.utils;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReverseTest {

    @Test
    void arrReverse() {
        Integer[] actual = {};

        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{}, actual);

        actual = new Integer[]{1};
        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{1}, actual);

        actual = new Integer[]{1, 2};
        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{2, 1}, actual);

        actual = new Integer[]{1, 2, 3};
        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{3, 2, 1}, actual);

        actual = new Integer[]{1, 2, 3, 4};
        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{4, 3, 2, 1}, actual);

        actual = new Integer[]{1, 2, 3, 4, 5};
        Reverse.reverse(actual);
        assertArrayEquals(new Integer[]{5, 4, 3, 2, 1}, actual);
    }

    @Test
    void listReverse() {
        var actual = Arrays.asList();

        Reverse.reverse(actual);

        assertEquals(Arrays.asList(), actual);

        actual = Arrays.asList(1);
        Reverse.reverse(actual);
        assertEquals(Arrays.asList(1), actual);

        actual = Arrays.asList(1, 2);
        Reverse.reverse(actual);
        assertEquals(Arrays.asList(2, 1), actual);

        actual = Arrays.asList(1, 2, 3);
        Reverse.reverse(actual);
        assertEquals(Arrays.asList(3, 2, 1), actual);

        actual = Arrays.asList(1, 2, 3, 4);
        Reverse.reverse(actual);
        assertEquals(Arrays.asList(4, 3, 2, 1), actual);

        actual = Arrays.asList(1, 2, 3, 4, 5);
        Reverse.reverse(actual);
        assertEquals(Arrays.asList(5, 4, 3, 2, 1), actual);
    }
}