package com.ti.fp.utils;

import java.util.Objects;

public class Point implements MyComparable<Point> {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public CompareResult compareTo(Point pt) {
        if (y == pt.y) {
            if (x > pt.x) return CompareResult.GREATER;
            if (x < pt.x) return CompareResult.LESS;
            return CompareResult.EQUAL;
        }
        if (y > pt.y) return CompareResult.GREATER;
        return CompareResult.LESS;
    }
}
