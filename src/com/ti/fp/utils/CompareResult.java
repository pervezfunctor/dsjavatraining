package com.ti.fp.utils;

public enum CompareResult {
    LESS, EQUAL, GREATER;

    static CompareResult from(int i) {
        return i < 0 ? LESS : i > 0 ? GREATER : EQUAL;
    }
}
