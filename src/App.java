import com.ti.fp.expr.BigIntegerArithmetic;
import com.ti.fp.expr.DoubleArithmetic;
import com.ti.fp.expr.Expression;
import com.ti.fp.utils.Reverse;
import com.ti.fp.utils.Reversible;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.ti.fp.seq.Sink.toList;
import static com.ti.fp.seq.Source.fromArrayList;
import static com.ti.fp.utils.Appendable.arrayAppender;
import static java.math.BigInteger.valueOf;

public class App {
    static void seqMain() {
        var arr = new ArrayList<Integer>();
        arr.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        var seq = fromArrayList(arr)
                .filter(x -> x % 2 == 0)
                .map(x -> x * x);

        var result = new ArrayList<Integer>();

        for (var i : arr) {
            if (i % 2 == 0)
                result.add(i * i);
        }

        System.out.println(toList(seq));
        System.out.println(toList(seq));
    }

    static void exprMain() {
        var e = new Expression<>(new BigIntegerArithmetic());

        var expr = e.plus(
                e.minus(e.op(valueOf(30)), e.op(valueOf(50))),
                e.negate(e.op(valueOf(30))));

        System.out.println(expr.eval());

        var de = new Expression<>(new DoubleArithmetic());

        var dExpr = de.plus(
                de.minus(30.0, 50.5),
                de.negate(30.2));

        System.out.println(dExpr.eval());
    }

    static void utilsMain() {
        var arr = new ArrayList<Integer>();
        arr.addAll(List.of(1, 2, 3, 4, 5, 6));

        Reverse.reversed(Reversible.from(arr), arrayAppender());
    }

    public static void main(String[] args) {


    }
}

